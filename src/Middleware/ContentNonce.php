<?php
namespace MegaXLR\LaravelCSP\Middleware;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

class ContentNonce
{
        public function handle(Request $request, \Closure $next)
        {
                $this->setNonce();

                $response = $next($request);
                $this->setHeaderCSP($response);
                return $response;
        }


        private function setNonce()
        {
                session()->flash('content_nonce', $this->generateNonce());
        }


        private function generateNonce()
        {
                return Str::random(64);
        }


        private function setHeaderCSP(Response $response)
        {
                $response->header('Content-Security-Policy', $this->nonceHeader());
                return $response;
        }


        private function nonceHeader()
        {
                return sprintf('script-src \'nonce-%s\'', $this->getNonce());
        }


        private function getNonce()
        {
                return session()->get('content_nonce');
        }
}
