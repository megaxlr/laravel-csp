<?php


namespace MegaXLR\LaravelCSP\Providers;


use Illuminate\Support\ServiceProvider;
use MegaXLR\LaravelCSP\Middleware\ContentNonce;

class CSPProvider extends ServiceProvider
{
        protected $defer = true;

        public function register()
        {
                $this->app->singleton(ContentNonce::class, function(){
                        return new ContentNonce();
                });
        }


        public function provides()
        {
                return [ContentNonce::class];
        }
}