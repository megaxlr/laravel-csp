<?php


if (!function_exists('nonce')) {
        function nonce()
        {
                return session('content_nonce');
        }
}

if(!function_exists('nonce_attr')) {
        function nonce_attr()
        {
                $attr  = sprintf('nonce="%s"', e(nonce()));
                $string = new \Illuminate\Support\HtmlString($attr);
                return $string;
        }
}